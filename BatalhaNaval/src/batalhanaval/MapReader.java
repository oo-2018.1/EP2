package batalhanaval;


import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import javax.swing.JOptionPane;

public class MapReader {
    
    private java.util.List<String> fileLine = new ArrayList<>();
    private static int mapNumberOfRows;
    private static int mapNumberOfLines;
    private Integer[] numberOfShips;
    private Integer[][] gameMap;

    
    public MapReader(String arquivo){

                    Scanner file = null;
                try {
                    file = new Scanner(new File(arquivo));
                    while(file.hasNextLine()){
                        fileLine.add(file.nextLine());
                    }
                } catch (FileNotFoundException e) {
                    JOptionPane.showMessageDialog(null, "Não foi possivel abrir o arquivo;");
                }


            String[] dimensionMatrix = fileLine.get(1).split(" ");
            mapNumberOfRows = Integer.parseInt(dimensionMatrix[0]);
            mapNumberOfLines = Integer.parseInt(dimensionMatrix[1]);


            gameMap = new Integer[mapNumberOfLines][mapNumberOfRows];

            for (int i = 0; i < mapNumberOfLines; i++){
                for (int j = 0; j < mapNumberOfRows; j++){
                    String line = fileLine.get(i+4);
                    gameMap[j][i] = (int) (line.charAt(j) - '0');
                }
            }

            numberOfShips = new Integer[5];

            for (int i = 0; i < 5; i++){
                String[] ship = fileLine.get(i+mapNumberOfLines+6).split(" ");
                numberOfShips[i] = Integer.parseInt(ship[1]);
            }
    }


public static int getMapNumberOfRows(){
    return mapNumberOfRows;
}

public static int getMapNumberOfLines(){
    return mapNumberOfLines;
}

public Integer[][] getGameMap(){
    return gameMap;
}

public Integer[] getNumberOfShips(){
    return numberOfShips;
}

}