package batalhanaval;

public class GameScore {

    public static int RESOURCES;
    private static int BOATS;
    
    //Habilities in game
    public static final int SHOT = 1 * 100;
    public static final int BOMB = 5 * 100;
    public static int LINE;
    public static int ROW;
    
    private final MapReader map = new MapReader(FileFound.mapName);
    private final Habilities skills = new Habilities(map);
    
    public GameScore(){
       setBoats();
       setLINE();
       setROW();
       teste();
       RESOURCES = ((MapReader.getMapNumberOfLines() * MapReader.getMapNumberOfRows() - BOATS + 10)/2) * 100;
    }
    
    public final void setBoats(){
        for(int i = 0; i < 5; i++){
          BOATS += skills.getNumberOfBoats(i);
        }
    }
    
    public final void setLINE(){
        LINE = (MapReader.getMapNumberOfLines() + 1) * 100;
    }
    
    public final void setROW(){
        ROW = (MapReader.getMapNumberOfRows() + 1) * 100;
    }
    
    public boolean callScore(int attack){
        switch (attack) {
            case Habilities.SHOT:
                if ((RESOURCES - SHOT) >= 0){
                    RESOURCES -= SHOT;
                    return true;
                }
                break;
            case Habilities.SHOTMATRIX:
                if ((RESOURCES - BOMB) >= 0){
                    RESOURCES -= BOMB;
                    return true;
                }
                break;
            case Habilities.SHOTLINE:
                if ((RESOURCES - LINE) >= 0){
                    RESOURCES -= LINE;
                    return true;
                }
                break;
            case Habilities.SHOTROW:
                if ((RESOURCES - ROW) >= 0){
                    RESOURCES -= ROW;
                    return true;
                }
                break;
            default:
                break;
        }
        return false;
    }
    
    public void teste(){
        System.out.println(MapReader.getMapNumberOfLines());
        System.out.println(MapReader.getMapNumberOfRows());
    }
}
