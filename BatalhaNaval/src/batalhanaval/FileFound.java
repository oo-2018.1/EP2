
package batalhanaval;

import java.io.File;
import java.io.FileNotFoundException;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

public class FileFound {
        public static String mapName = "";
        
    public void buscar() throws FileNotFoundException{
        boolean occurredAnError = false;
        JFileChooser chooser = new JFileChooser();

        FileNameExtensionFilter filter = new FileNameExtensionFilter("Arquivo TXT", "txt");
        chooser.setFileFilter(filter);


        if(mapName == ""){
            int returnVal = chooser.showOpenDialog(null);
                if(returnVal == JFileChooser.APPROVE_OPTION) {
                    try{
                            mapName = chooser.getSelectedFile().getAbsolutePath();
                        } catch(Exception e){
                            occurredAnError = true;
                            JOptionPane.showMessageDialog(null, "Selecione um mapa");
                        }

                        if(!occurredAnError){
                            try{
                                MapReader file = new MapReader(mapName);
                                CanvasInterface frame = new CanvasInterface();
                                frame.setVisible(true);
                            } catch(Exception e){
                                e.printStackTrace();
                              }
                        }

                }
            }
        }
 }
