package batalhanaval;

public class CanvasThread extends Thread{
    private CanvasGame canvas;
    private boolean running = true;
    
    public CanvasThread(CanvasGame canvas){
        this.canvas = canvas;
    }
    
    @Override
    public void run(){
        while(running){
            try{
                Thread.sleep(100);
            } catch(InterruptedException e){
                e.printStackTrace();
            }
            canvas.paint(canvas.getGraphics());
        }
    }
}
