
package batalhanaval;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JTextArea;


public final class CanvasGame extends Canvas{
    public final int RECT_WIDTH;
    public final int RECT_HEIGTH;
    public static final int MARGIN = 1;
    public static final int MENU = 250;
    
    private Habilities skill;
    
    private int mapNumberOfRows;
    private int mapNumberOfLines;
    
    JTextArea Text = new JTextArea();
    
    public CanvasGame(Habilities skill){
        this.skill = skill;
        mapNumberOfRows = MapReader.getMapNumberOfRows();
        mapNumberOfLines = MapReader.getMapNumberOfLines();
        RECT_WIDTH = (CanvasInterface.C_WIDTH - MENU-MARGIN) / mapNumberOfRows;
        RECT_HEIGTH = (CanvasInterface.C_HEIGTH-MARGIN) / mapNumberOfLines;
        
    }
    @Override
    public void paint(Graphics g){
        //Prepare an ImageIcon
        ImageIcon icon = new ImageIcon("gifs/ocean.gif");
        ImageIcon iconShot = new ImageIcon("gifs/explosion.gif");
        ImageIcon iconWater = new ImageIcon("gifs/explosionWater.gif");
//        ImageIcon iconSearch = new ImageIcon("images/looking.gif");
//        
        final Image img = icon.getImage();
        final Image imgShot = iconShot.getImage();
        final Image imgWater = iconWater.getImage();
//        final Image imgSearch = iconSearch.getImage();
            
        for(int i = 0; i < mapNumberOfRows; i++){
            for(int j = 0; j < mapNumberOfLines; j++){
                g.drawImage(img, i*RECT_WIDTH+MARGIN, j*RECT_HEIGTH+MARGIN, RECT_WIDTH, RECT_HEIGTH, null);
                if(skill.getChoices(i, j) == 1){
                    g.drawImage(imgShot, i*RECT_WIDTH+MARGIN, j*RECT_HEIGTH+MARGIN, RECT_WIDTH, RECT_HEIGTH, null);
                }
                if(skill.getChoices(i, j) == -1){
                    g.drawImage(imgWater, i*RECT_WIDTH+MARGIN, j*RECT_HEIGTH+MARGIN, RECT_WIDTH, RECT_HEIGTH, null);
                }
            }
        }     
    }
}