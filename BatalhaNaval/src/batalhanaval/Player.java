package batalhanaval;

public final class Player {
    private int resources;
    private CanvasGame canvas;
    
    public Player(CanvasGame canvas){
        
        int sum = 0;
        for(int i = 0; i < 5; i++){
            sum += canvas.numberOfShips[i];
        }
        resources = (canvas.getCanvasNumberOfRows() * canvas.getCanvasNumberOfLines() - sum + 10)/2;
        
        setResources(resources);
    }
    
public int getResources(){
    return resources;
}

public void setResources(int resources){
    this.resources = resources;
}
}
