
package batalhanaval;

import static batalhanaval.GameScore.RESOURCES;
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;


public class CanvasInterface extends JFrame {
    
    public static final int C_WIDTH = 800;
    public static final int C_HEIGTH = 600;
    
    private CanvasGame canvas;
    CanvasThread updateScreenThread;
    private final MapReader mapInfo;
    private Habilities skills;
    private GameScore gameScore;
    
    private int x1_pos, x2_pos;
    private int y1_pos, y2_pos;

    private JTextArea Text = new JTextArea();
    
    public CanvasInterface(){
        mapInfo = new MapReader(FileFound.mapName);
        gameScore = new GameScore();
        skills = new Habilities(mapInfo);
        canvas = new CanvasGame(skills);
        updateScreenThread = new CanvasThread(canvas);

        
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        setLocationRelativeTo(null);
        
        getContentPane().setLayout(new BorderLayout());
        setTitle("Batalha Naval 2.0");        
        

        setSize(C_WIDTH, C_HEIGTH);
        
        getContentPane().add("Center", canvas);
        
        setVisible(true);
        
        updateScreenThread.start();
       
        canvas.addMouseListener(new MouseListener(){
            
            @Override
            public void mouseReleased(MouseEvent e){
                int x = e.getX();
                int y = e.getY();
                
                x2_pos = x/canvas.RECT_WIDTH;
                y2_pos = y/canvas.RECT_HEIGTH;
                if(!skills.winner() && !skills.looser()){
                    int attack = -1;
                    try{
                        attack = skills.setSkill(x1_pos, y1_pos, x2_pos, y2_pos);
                    }catch(Exception e2){
                        JOptionPane.showMessageDialog(null, "Jogada não válida, tente novamente!");
                    }
                    
                    if(attack == -1){
                        JOptionPane.showMessageDialog(null, "Área não válida, tente novamente!");
                    }
                    else{
                        if(gameScore.callScore(attack)){
                            skills.setChoices(x1_pos, y1_pos, x2_pos, y2_pos);
                        }
                        else{
                            JOptionPane.showMessageDialog(null, "Você não tem mais WarCoins para lançar essa habilidade!");
                        }
                    }
                }
                
                if(skills.winner()){
                    JOptionPane.showMessageDialog(null, "Você ganhou!!!\nClique em voltar para retornar ao Menu Inicial!");
                }
                else{
                    if(skills.looser()){
                        JOptionPane.showMessageDialog(null, "Ah que pena, não foi dessa vez\nClique em voltar e tente novamente!");
                    }
                }
                
            }

            @Override
            public void mouseClicked(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
                int x = e.getX();
                int y = e.getY();
                
                x1_pos = x/canvas.RECT_WIDTH;
                y1_pos = y/canvas.RECT_HEIGTH;
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
           
        });
    }
 }