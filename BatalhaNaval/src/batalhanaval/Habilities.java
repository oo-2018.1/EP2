package batalhanaval;

import static java.lang.Math.abs;

public class Habilities {
    public static final int SHOT = 1;
    public static final int SHOTMATRIX = 2;
    public static final int SHOTLINE = 3;
    public static final int SHOTROW = 4;
    public static final int SEARCH = 5;
    
    private MapReader mapInfo;
    
    private int[][] choices;
    private int[][] boolChoices;
    private int[][] boolBoats;
    
    private Integer[][] mapGame;
    private Integer[] numberOfBoats;
    
    private int mapNumberOfLines;
    private int mapNumberOfRows;
    
    public Habilities(MapReader map){
        this.mapInfo = map;
        mapGame = mapInfo.getGameMap();
        numberOfBoats = mapInfo.getNumberOfShips();
        mapNumberOfLines = MapReader.getMapNumberOfLines();
        mapNumberOfRows = MapReader.getMapNumberOfRows();
        
        initMatrix();
    }
    
    public void initMatrix(){
        choices = new int[mapNumberOfLines][mapNumberOfRows];
        for(int i = 0; i < mapNumberOfLines; i++){
            for(int j = 0; j < mapNumberOfRows; j++){
                choices[i][j] = 0;
            }
        }
        
        boolChoices = new int[mapNumberOfLines][mapNumberOfRows];
        for(int i = 0; i < mapNumberOfLines; i++){
            for(int j = 0; j < mapNumberOfRows; j++){
                boolChoices[i][j] = 0;
            }
        }
        
        boolBoats = new int[mapNumberOfLines][mapNumberOfRows];
        for(int i = 0; i < mapNumberOfLines; i++){
            for(int j = 0; j < mapNumberOfRows; j++){
                boolBoats[i][j] = 0;
            }
        }
    }
    
    public int setSkill(int x1, int y1, int x2, int y2){
        int x = abs(x2-x1);
        int y = abs(y2-y1);
        
        if((x == 0 && y == 0) && (choices[x1][y1] == 1 || choices[x1][y1] == -1)){
            return -1;
        }
        else if(x == 0 && y == 0){
            return SHOT;
        }
        else if(x == 1 && y == 1){
            return SHOTMATRIX;
        }
        else if(x == 0 && y == mapNumberOfRows-1){
            return SHOTLINE;
        }
        else if(y == 0 && x == mapNumberOfLines-1){
            return SHOTROW;
        }
        if((x == 0 && y == 0) && (mapGame[x1][y1] == 1)){
            return -1;
        }
        else{
            return -1;
        }
    }
    
    public void setChoices(int x1, int y1, int x2, int y2){
        int a;
        int b;
        int c;
        int d;
        
        if(x1 >= x2){
            a = x1;
            b = x2;
        }
        else{
            a = x2;
            b = x1;
        }
        
        if(y1 >= y2){
            c = y1;
            d = y2;
        }
        else{
            c = y2;
            d = y1;
        }
        
        for(int i = b; i <= a; i++){
            for(int j = d; j <= c; j++){
                if(mapGame[i][j] > 0){
                    choices[i][j] = 1;
                    boolChoices[i][j] = 1;
                }
                else{
                    choices[i][j] = -1;
                }
            }
        }
        
        DestroyedLines();
        DestroyedRows();
    }
    
    public boolean winner(){
        boolean win = true;
        externfor: for(int i = 0; i < mapNumberOfLines; i++){
            for(int j = 0; j < mapNumberOfRows; j++){
                if(choices[i][j] != 1 && mapGame[i][j] > 0){
                    win = false;
                    break externfor;
                }
            }
        }
        
//        if(win == true){
//            RankMaker rank = new RankMaker();
//            rank.readBoard();
//            rank.sortBoard();
//            rank.writeBoard();
//        }
        return win;
    }
    
public boolean looser(){
    return (GameScore.RESOURCES - GameScore.SHOT) < 0;
}
    
    public void DestroyedLines(){
        for (int i = 0; i < mapNumberOfLines; i++){
            int point = 0;
            int copyPoint = 0;
                        
            for (int j = 0; j < mapNumberOfRows; j++){
                if (mapGame[i][j] != 0 && boolChoices[i][j] == 1 && boolBoats[i][j] == 0){
                    if (copyPoint != mapGame[i][j] || point == 0){
                        point = mapGame[i][j]+1;
                        copyPoint = mapGame[i][j];
                    }
                }
                                
                if (mapGame[i][j] == 0){
                    point = 0;
                    copyPoint = 0;
                }
                                
                if (point > 0 && boolChoices[i][j] == 1){
                    point--;
                }
                                
                if (point == 1){
                    if (copyPoint == 1){
                        numberOfBoats[0] -= 1;
                        boolBoats[i][j] = 1;
                    }
                    else if (copyPoint == 2){
                        numberOfBoats[1] -= 1;
                        boolBoats[i][j] = 1;
                        boolBoats[i][j-1] = 1;
                    }
                    else if (copyPoint == 3){
                        numberOfBoats[2] -= 1;
                        boolBoats[i][j] = 1;
                        boolBoats[i][j-1] = 1;
                        boolBoats[i][j-2] = 1;
                    }
                    else if (copyPoint == 4){
                        numberOfBoats[3] -= 1;
                        boolBoats[i][j] = 1;
                        boolBoats[i][j-1] = 1;
                        boolBoats[i][j-2] = 1;
                        boolBoats[i][j-3] = 1;
                    }
                    else if (copyPoint == 5){
                        numberOfBoats[4] -= 1;
                        boolBoats[i][j] = 1;
                        boolBoats[i][j-1] = 1;
                        boolBoats[i][j-2] = 1;
                        boolBoats[i][j-3] = 1;
                        boolBoats[i][j-4] = 1;
                    }
                                        
                    point = 0;
                    copyPoint = 0;
                }
                                
            }     
        }           
    }
    
    public void DestroyedRows(){
        for (int i = 0; i < mapNumberOfRows; i++){
            int point = 0;
            int copyPoint = 0;
                        
            for (int j = 0; j < mapNumberOfLines; j++){
                if (mapGame[i][j] != 0 && boolChoices[i][j] == 1 && boolBoats[i][j] == 0){
                    if (copyPoint != mapGame[i][j] || point == 0){
                        point = mapGame[i][j]+1;
                        copyPoint = mapGame[i][j];
                    }
                }
                                
                if (mapGame[i][j] == 0){
                    point = 0;
                    copyPoint = 0;
                }
                                
                if (point > 0 && boolChoices[i][j] == 1){
                    point--;
                }
                                
                if (point == 1){
                    if (copyPoint == 1){
                        numberOfBoats[0] -= 1;
                        boolBoats[i][j] = 1;
                    }
                    else if (copyPoint == 2){
                        numberOfBoats[1] -= 1;
                        boolBoats[i][j] = 1;
                        boolBoats[i][j-1] = 1;
                    }
                    else if (copyPoint == 3){
                        numberOfBoats[2] -= 1;
                        boolBoats[i][j] = 1;
                        boolBoats[i][j-1] = 1;
                        boolBoats[i][j-2] = 1;
                    }
                    else if (copyPoint == 4){
                        numberOfBoats[3] -= 1;
                        boolBoats[i][j] = 1;
                        boolBoats[i][j-1] = 1;
                        boolBoats[i][j-2] = 1;
                        boolBoats[i][j-3] = 1;
                    }
                    else if (copyPoint == 5){
                        numberOfBoats[4] -= 1;
                        boolBoats[i][j] = 1;
                        boolBoats[i][j-1] = 1;
                        boolBoats[i][j-2] = 1;
                        boolBoats[i][j-3] = 1;
                        boolBoats[i][j-4] = 1;
                    }
                                        
                    point = 0;
                    copyPoint = 0;
                }
                                
            }     
        }           
    }
    public Integer[][] mapGame(){
        return mapGame;
    }
    
    public Integer getNumberOfBoats(Integer a){
        return numberOfBoats[a];
    }
    
    public int getChoices(int x, int y){
        return choices[x][y];
    }
}
